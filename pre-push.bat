@echo off
::===============================================================
@REM This Script is called by the pre-push hook.
@REM Ensure the Script doesn't have any prompts.
@REM The Script below copies and compresses the new files and directories in .\venv\Lib\site-packages to ..\..\site_packages.tar.
@REM At last, new files will be added to git and committed.
::===============================================================

echo.
echo pre-push.bat activated.

@REM  Displaying current checking branch
git branch 

@REM Compressing site-packages dir

echo.
echo Compressing packages...

@REM Checking the existence of the site-packages directory 
if not exist venv\Lib\site-packages goto error
cd venv\Lib\
tar -cvf ..\..\site_packages.tar site-packages
cd ..\..\
goto aftercompress

:aftercompress

@REM Updating requirements.txt
echo.
echo Generating requirements.txt
start "" /wait cmd.exe /c make-reqs.bat

@REM Adding site_packages.tar and .idea/
@REM Here adding both site_packages.tar and .idea(because of auto setting pycharm configuration)


echo.
echo Git adding
git add --force site_packages.tar
git add --force .idea/
git add requirements.txt

echo.
echo Git commiting
git commit -m "commited by pre-push hook"

pause
cls
echo.
echo libs are copied, added and commited...
goto success

:error
exit /b 1
:success
exit /b 0


:exiting_script
echo.
echo exiting...
pause
exit