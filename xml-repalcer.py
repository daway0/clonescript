"""
this .py file is in .idea directory
"""


import os


def replace_in_file(file_path, search_text, new_text):

    with open(file_path+"temp.xml", 'w') as wf:
        with open(file_path+"workspace.xml", 'r') as rf:
            
            for line in rf:
                
                
                if search_text in line:
                    new_line = f'      <option name="SDK_HOME" value="{new_text}" />'
                    wf.write(new_line)

                else:
                    wf.write(line)


if __name__ == "__main__":

    
    py_path = os.path.join(os.getcwd(), "python.exe")
    
    replace_in_file("..\\..\\.idea\\",
                    'name="SDK_HOME"',
                    py_path)
