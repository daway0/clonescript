@REM ===============================================================
@REM The Script below helps us to clone repositories,
@REM  create a virtual environment, copy hooks, and Config the PyCharm configuration at the end.

@REM ***File structure MUST be like this: ***
@REM .
@REM ├── DjangoProject
@REM │   ├── CloneScript
@REM │   	├── auto_cloner.bat  <<< JUST RUN ME ! >>>
@REM │		├── make-reqs.bat
@REM │		├── post-merge
@REM │		├── post-merge.bat
@REM │		├── pre-push
@REM │		├── pre-push.bat
@REM │		├── README.md
@REM │   	└── xml-repalcer.py
@REM │   ├── Porject1
@REM │   ├── Porject2
@REM │   ├── Porject3
@REM │   ├── Porject4
@REM │   └── Project5

@REM ***Before use***
@REM 1) Make sure that python 3.9 (3.9.7), git, pip, and tar are installed on your machine by testing them in command prompt or PowerShell.

@REM 2) It assumes that you don't have any c files that should use in the module, so it ignores vevn\Include directory

@REM 3) Using a terminal(git Bash preferred) rather than PyCharm GUI for git commands is better. (just a recommendation :) )

@REM There are 5 main actions here;
@REM Cloning 
@REM Creating virtual environment 
@REM Copying batch files and hooks
@REM Decompressing site_packages
@REM Configuring PyCharm config
@REM ===============================================================

@echo off 
cls
setlocal enabledelayedexpansion
set count=0

@REM Adding new project: You must manually add the project name
@REM in the Projects array(Case sensitive).
@REM Make sure the project name is the same as the Azure DevOps repository name.
@REM WARNING: sort project names alphabetically

@REM Projects name array
set Projects= AccessControl ^
ChangeTeamRole ^
CostPrice ^
Documentation ^
EIT ^
ErfanSandbox ^
Evaluation ^
FAQ ^
HR ^
LeavingSession ^
Negotiate ^
PersonnelService ^
Portal ^
ProcessManagement ^
ServiceBook ^
Survey ^
SystemHelp ^
TeamFeature ^
TeamWorkPlan ^ 

@REM Displaying the project list and Getting user input 
for %%0 in (%Projects%) do (
    set /a count=count+1
    set choice[!count!]=%%0
)

echo.
echo.

for /l %%x in (1,1,!count!) do (
    echo [%%x] !choice[%%x]!
)

echo.
set /p chose=Which project u wanna clone?


cls 


@REM Cloning selected repository

@REM Checking the existence of the selected project directory in ..\
echo.
echo !choice[%chose%]! selected.

if exist ..\!choice[%chose%]! (
	echo.
	
	echo The !choice[%chose%]! already exists. Delete ..\!choice[%chose%]! before re-cloning, then re-run the script.
	echo.
	echo OR
	echo.
	echo Continue and let me make venv with the existing cloned project. 

	goto :afterclone
	
)

echo.
echo Cloning...

echo.
git clone http://eit-django-ver/DJANGO/!choice[%chose%]!/_git/!choice[%chose%]! ..\!choice[%chose%]!

:afterclone
@REM Creating virtual environment
@REM WARNING: Creating venv and Copying required files phases are together (continuously). If creating venv is skipped, copying files will be missed too.

@REM Checking the existence of the venv directory 
if exist ..\!choice[%chose%]!\venv (
	echo.
	echo !choice[%chose%]!\venv exists.
)

echo.
set /p chosevenv=Do you want to create venv(y/n/d:delete venv)?
if %chosevenv% == n goto aftervenv
if %chosevenv% == d goto deletevenv
goto createvenv

:deletevenv
if not exist ..\!choice[%chose%]!\venv (
	echo.
	echo There is no venv.
	goto :afterclone
)
echo.
echo Removing venv dir...
rmdir /q/s ..\!choice[%chose%]!\venv
goto afterclone

:createvenv
echo.
echo Creating venv. It takes a while... 

py -m venv ..\!choice[%chose%]!\venv

echo.
echo Project venv tree
tree ..\!choice[%chose%]!\venv\Scripts /f


@REM Copying .bat files and hooks  

@REM pre-push hook
echo.
echo Copying pre-push hook... 
xcopy pre-push ..\!choice[%chose%]!\.git\hooks /y /d

@REM post-merge (post-pull) hook
echo.
echo Copying post-merge hook... 
xcopy post-merge ..\!choice[%chose%]!\.git\hooks /y /d

@REM pre-push.bat
echo.
echo Copying pre-push.bat...
xcopy pre-push.bat ..\!choice[%chose%]!\venv\Lib /y /d

@REM post-merge.bat
echo.
echo Copying post-merge.bat...
xcopy post-merge.bat ..\!choice[%chose%]! /y /d

@REM make-reqs.bat
echo.
echo Copying make-reqs.bat... 
xcopy make-reqs.bat ..\!choice[%chose%]! /y /d

:aftercopy
:aftervenv
@REM Decompressing site_packages.tar

if not exist ..\!choice[%chose%]!\site_packages.tar (
	echo.
	echo site_packages.tar not found.	
	goto :afterdecompress)
echo.
set /p chosedecompress=Do you want to decompress site_packages(y/n)?
if %chosedecompress% == n goto afterdecompress
echo.
echo Decompressing site_packages.tar
tar -xvf ..\!choice[%chose%]!\site_packages.tar -C ..\!choice[%chose%]!\venv\Lib

:afterdecompress
@REM Configuring PyCharm config

if not exist ..\!choice[%chose%]!\.idea (
	echo.
	echo .idea directory not found.
	goto :afterpycharmconfig)

@REM Copying xml_replacer.py 
echo.
echo Copying xml-replacer.py... 
xcopy xml-repalcer.py ..\!choice[%chose%]!\.idea /y /d

echo.
set /p chosepycharmconfig=Do you want to configuration pycharm(y/n)?
if %chosepycharmconfig% == n goto afterpycharmconfig

echo.
echo Setting Pycharm Configs... 
cd ..\!choice[%chose%]!\venv\Scripts
py ..\..\.idea\xml-repalcer.py
cd ..\..\
del .idea\workspace.xml
ren .idea\temp.xml workspace.xml

:afterpycharmconfig
pause 
cls

echo.	
echo Donate Erfan a coffee for saving your time and sanity :)

:exiting_script
echo.
pause
exit