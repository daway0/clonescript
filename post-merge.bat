@echo off

@REM Decompressing site_packages.tar

if not exist site_packages.tar goto :afterdecompress
echo.
echo Decompressing site_packages.tar
tar -xvf site_packages.tar -C venv\Lib
goto success

:afterdecompress
echo. 
echo cannot find the site_packages.tar

:success
exit /b 0